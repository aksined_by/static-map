<?php

declare(strict_types=1);

const IS_APP_STARTED_VALID = true;

require_once __DIR__ . '/config.php';
require_once __DIR__ . '/routes.php';
require_once __DIR__ . '/functions.php';

$method = strtolower(\Core\Router\Router::getMethod() ?? '');

if (!$method) {
    show_404();
}

\Core\Router\Router::$method();
