<?php

declare(strict_types=1);

namespace Core\Database;

use Core\Exception\DatabaseException;
use mysqli;

class MySql implements ConnectionInterface
{
    private static false|mysqli $connection = false;

    private static ?MySQl $instance = null;

    private function __construct()
    {
        self::$connection = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME, DB_PORT);
    }

    public function __destruct()
    {
        if (self::$connection) {
            mysqli_close(self::$connection);
        }
    }

    public static function connect(): false|mysqli
    {
        if (!self::$instance) {
            self::$instance = new MySql();
        }

        if (!self::$connection) {
            throw new DatabaseException('Connection fail');
        }

        return self::$connection;
    }

    public static function select(string $query): array
    {
        if (!self::$connection) {
            self::connect();
        }

        $response = self::$connection->query($query);
        $rows     = [];

        while ($row = $response->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    public static function insert(string $tableName, array $parameters): bool
    {
        $numberParameters = count($parameters);

        if (!$numberParameters) {
            throw new DatabaseException('Insert with empty parameters');
        }

        if (!self::$connection) {
            self::connect();
        }

        $query = sprintf(
            "INSERT INTO $tableName (%s) VALUES (%s)",
            implode(',', array_keys($parameters)),
            implode(',', array_fill(0, $numberParameters, '?')),
        );

        $stmt = self::$connection->prepare($query);

        $stmt->bind_param(str_repeat('s', $numberParameters), ...array_values($parameters));

        return $stmt->execute();
    }
}