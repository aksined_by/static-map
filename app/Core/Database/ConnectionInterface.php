<?php

declare(strict_types=1);

namespace Core\Database;

use Core\Exception\DatabaseException;

interface ConnectionInterface
{
    /**
     * @throws DatabaseException
     */
    public static function connect();

    /**
     * @throws DatabaseException
     */
    public static function select(string $query): array;

    /**
     * @throws DatabaseException
     */
    public static function insert(string $tableName, array $parameters): bool;
}
