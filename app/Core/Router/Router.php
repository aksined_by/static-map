<?php

declare(strict_types=1);

namespace Core\Router;

use Core\Exception\BadRequestException;

class Router extends RouterAbstract
{
    public static function get()
    {
        if (self::getMethod() !== self::GET) {
            throw new BadRequestException();
        }

        [$controller, $method] = self::handle();

        return $controller->$method();
    }

    public static function post()
    {
        if (self::getMethod() !== self::POST) {
            throw new BadRequestException();
        }

        [$controller, $method] = self::handle();

        return $controller->$method();
    }
}
