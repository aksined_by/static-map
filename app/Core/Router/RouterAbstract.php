<?php

declare(strict_types=1);

namespace Core\Router;

use Core\Exception\BadRequestException;
use Throwable;

abstract class RouterAbstract implements RouterInterface
{
    public static function getMethod(): ?string
    {
        return isset(self::VALID_METHODS[$_SERVER['REQUEST_METHOD']]) ? $_SERVER['REQUEST_METHOD'] : null;
    }

    /**
     * @throws BadRequestException
     */
    public static function getRouteHandler(): ?array
    {
        $parsedUrl = parse_url($_SERVER['REQUEST_URI']);

        $handler = ROUTES[$parsedUrl['path']] ?? null;

        if (!$handler) {
            return null;
        }

        try {
            [$class, $method] = explode('::', $handler);
        } catch (Throwable) {
            throw new BadRequestException();
        }

        return [$class, $method];
    }

    /**
     * @throws BadRequestException
     */
    protected static function handle()
    {
        [$class, $method] = self::getRouteHandler();

        if (!$class || !$method) {
            throw new BadRequestException();
        }

        if (class_exists($class)) {
            $controller = new $class();

            if (!method_exists($controller, $method)) {
                throw new BadRequestException("Method $class::$method not found");
            }
        } else {
            throw new BadRequestException("Class $class not found");
        }

        return [$controller, $method];
    }
}
