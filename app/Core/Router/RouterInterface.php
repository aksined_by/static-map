<?php

declare(strict_types=1);

namespace Core\Router;

use Core\Exception\BadRequestException;

interface RouterInterface
{
    public const GET  = 'GET';
    public const POST = 'POST';

    public const VALID_METHODS = [
        self::GET  => self::GET,
        self::POST => self::POST,
    ];

    /**
     * @throws BadRequestException
     */
    public static function get();

    /**
     * @throws BadRequestException
     */
    public static function post();
}
