<?php

declare(strict_types=1);

namespace Core\Request;

class Request implements RequestInterface
{
    public static function ajaxPost(?string $key = null)
    {
        static $requestData;

        if (!$requestData) {
            $requestData = file_get_contents('php://input');
            $requestData = json_decode($requestData, true);
        }

        if ($key) {
            return $requestData[$key] ?? null;
        }

        return $requestData;
    }

    public static function post(?string $key = null)
    {
        if ($key) {
            return $_POST[$key] ?? null;
        }

        return $_POST;
    }

    public static function get(?string $key = null)
    {
        if ($key) {
            return $_GET[$key] ?? null;
        }

        return $_GET;
    }

    public static function ip(): ?string
    {
        return $_SERVER['REMOTE_ADDR'] ?? null;
    }
}
