<?php

declare(strict_types=1);

namespace Core\Curl;

use Core\Exception\CurlException;
use CurlHandle;

class Curl
{
    private static ?Curl $instance = null;

    private ?CurlHandle $curl;

    private function __construct()
    {
        $this->curl = curl_init();
    }

    public function __destruct()
    {
        if ($this->curl) {
            curl_close($this->curl);
        }
    }

    public static function connect(): Curl
    {
        if (!self::$instance) {
            self::$instance = new Curl();
        }

        return self::$instance;
    }

    public function setOption(int $option, mixed $value): self
    {
        curl_setopt($this->curl, $option, $value);

        return self::$instance;
    }

    /**
     * @throws CurlException
     */
    public function execute(): string
    {
        $result = curl_exec($this->curl);

        if (!$result) {
            throw new CurlException(curl_error($this->curl));
        }

        return $result;
    }
}
