<?php

declare(strict_types=1);

namespace Core\Response;

interface ResponseInterface
{
    public static function html(string $template, array $data): void;

    public static function json(array $data): void;
}
