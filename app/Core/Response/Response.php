<?php

declare(strict_types=1);

namespace Core\Response;

class Response implements ResponseInterface
{
    public static function html(string $template, array $data): void
    {
        view($template, $data);
    }

    public static function json(array $data): void
    {
        header('Content-Type: application/json; charset=utf-8');

        echo json_encode($data);
    }
}
