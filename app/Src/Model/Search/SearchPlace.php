<?php

declare(strict_types=1);

namespace Src\Model\Search;

use Core\Curl\Curl;
use Core\Router\RouterInterface;
use Src\Model\ModelInterface;

class SearchPlace implements ModelInterface
{
    public function findGoogleAutocompleteAddresses(string $input): array
    {
        $queryData = [
            'input' => $input,
            'types' => 'establishment',
            'key'   => GOOGLE_APY_KEY_MAP,
        ];

        $url = GOOGLE_API_AUTOCOMPLETE_ADDRESS . '?' . http_build_query($queryData);

        $response = Curl::connect()
            ->setOption(CURLOPT_URL, $url)
            ->setOption(CURLOPT_RETURNTRANSFER, 1)
            ->setOption(CURLOPT_FOLLOWLOCATION, 1)
            ->setOption(CURLOPT_CUSTOMREQUEST, RouterInterface::GET)
            ->setOption(CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
            ])
            ->execute();

        return json_decode($response, true);
    }

    public function getCoordinatesByPlaceId(string $placeId): array
    {
        $queryData = [
            'place_id' => $placeId,
            'key'      => GOOGLE_APY_KEY_MAP,
        ];

        $url = GOOGLE_API_ADDRESS_DETAILS . '?' . http_build_query($queryData);

        $response = Curl::connect()
            ->setOption(CURLOPT_URL, $url)
            ->setOption(CURLOPT_RETURNTRANSFER, true)
            ->execute();

        $placeDetailsData = json_decode($response, true);

        if (
            $placeDetailsData
            && isset($placeDetailsData['status'])
            && $placeDetailsData['status'] === 'OK'
            && isset($placeDetailsData['result']['geometry']['location'])
        ) {
            $latitude  = (string) $placeDetailsData['result']['geometry']['location']['lat'];
            $longitude = (string) $placeDetailsData['result']['geometry']['location']['lng'];
        } else {
            $latitude = $longitude = null;
        }

        return [$latitude, $longitude];
    }

    public static function getMapByCoordinates(?string $latitude, ?string $longitude): string
    {
        if (!$longitude || !$latitude) {
            return GOOGLE_API_MAP_STATIC;
        }

        $queryData = [
            'center'  => "$latitude,$longitude",
            'zoom'    => 13,
            'size'    => '900x900',
            'maptype' => 'roadmap',
            'markers' => "color:red|label:$latitude,$longitude",
            'key'     => GOOGLE_APY_KEY_MAP,
        ];

        return GOOGLE_API_MAP_STATIC . '?' . http_build_query($queryData);
    }
}