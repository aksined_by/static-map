<?php

declare(strict_types=1);

namespace Src\Model\Search;

use Core\Database\MySql;
use DateTime;
use Src\Model\ModelInterface;

class SearchHistory implements ModelInterface
{
    public const TABLE_NAME = 'search_history';

    public function getHistory(?string $ip, int $limit = 10): array
    {
        $query = 'SELECT * FROM ' . self::TABLE_NAME . " WHERE ip='$ip' ORDER BY date DESC LIMIT $limit";

        return MySql::select($query);
    }

    public function addHistory(string $ip, string $latitude, string $longitude, string $search): bool
    {
        return MySql::insert(self::TABLE_NAME, [
            'search'    => htmlspecialchars($search),
            'latitude'  => htmlspecialchars($latitude),
            'longitude' => htmlspecialchars($longitude),
            'ip'        => $ip,
            'date'      => (new DateTime())->format('Y-m-d H:i:s'),
        ]);
    }
}
