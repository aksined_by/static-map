<?php

declare(strict_types=1);

use Core\Router\RouterInterface;

?>
<script>


    document.addEventListener('DOMContentLoaded', function() {
        const searchInput = document.getElementById('searchInput');
        const resultsList = document.getElementById('resultsList');

        searchInput.addEventListener('input', function() {
            if (searchInput.value.length < 3) {
                resultsList.style.display = 'none';

                return;
            }

            resultsList.innerHTML = '';

            makeAjaxRequest(
                '/searchPlaces',
                '<?= RouterInterface::POST ?>',
                {
                    'search': searchInput.value
                },
                function (responseData) {
                    if (responseData && responseData.status === 'OK' && responseData.predictions) {
                        responseData.predictions.forEach(prediction => {
                            const listItem = document.createElement('li');

                            listItem.textContent = prediction.description;
                            listItem.addEventListener('click', function() {
                                searchInput.value         = prediction.description;
                                resultsList.style.display = 'none';

                                window.location.href = `/showLocation?placeId=${prediction.place_id}`
                                    + `&placeName=${prediction.description}`;
                            });
                            resultsList.appendChild(listItem);
                        });

                        resultsList.style.display = 'block';
                    }
                }
            );
        });

        document.addEventListener('click', function(event) {
            if (!resultsList.contains(event.target) && event.target !== searchInput) {
                resultsList.style.display = 'none';
            }
        });
    });
</script>