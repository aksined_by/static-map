<script>
    function makeAjaxRequest(url, method, data, successCallback, errorCallback) {
        const xhr = new XMLHttpRequest();

        xhr.open(method, url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status >= 200 && xhr.status < 300) {
                    if (successCallback) {
                        successCallback(JSON.parse(xhr.responseText));
                    }
                } else {
                    if (errorCallback) {
                        errorCallback(xhr.status, xhr.statusText);
                    }
                }
            }
        };

        if (data) {
            xhr.send(JSON.stringify(data));
        } else {
            xhr.send();
        }
    }
</script>