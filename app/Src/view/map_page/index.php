<?php

declare(strict_types=1);

use Src\Model\Search\SearchPlace;

view('header', ['title' => $title ?? 'title']);

?>
<div class="header-container">
    <h1><?= $header ?? 'Header' ?></h1>
</div>
<div class="container">
    <div class="form-container">
        <form id="searchForm" onsubmit="return false;">
            <label for="searchInput"></label>
            <input type="text" id="searchInput" placeholder="Введите место" value="<?= isset($search) ? htmlspecialchars_decode($search) : '' ?>">
            <div id="resultsContainer">
                <ul id="resultsList"></ul>
            </div>
        </form>
        <div>
            <?php if (!empty($history)) { ?>
                <ul>
                    <?php foreach ($history as $row) { ?>
                    <li><a href="/?latitude=<?=$row['latitude']?>&longitude=<?=$row['longitude']?>&search=<?=$row['search']?>"><?=$row['search']?></a> <?= $row['date'] ?></li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>
    </div>
    <div class="image-container">
        <img id="googleMap" src="<?= SearchPlace::getMapByCoordinates($latitude ?? null, $longitude ?? null) ?>" alt="Google Map">
    </div>
</div>
<?php

view('footer', ['scripts' => ['js/google/map_footer']]);
