<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $title ?? 'Project Page' ?></title>

    <link rel="stylesheet" href="Src/view/css/style.css">
    <?php view('js/ajax'); ?>

    <?php if (isset($scripts) && is_array($scripts)) {
        foreach ($scripts as $script) {
            view($script);
        }
    } ?>
</head>
<body>