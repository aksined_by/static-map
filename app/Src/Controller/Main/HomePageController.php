<?php

declare(strict_types=1);

namespace Src\Controller\Main;

use Core\Request\Request;
use Core\Response\Response;
use Exception;
use Src\Controller\Controller;
use Src\Model\Search\SearchHistory;
use Src\Model\Search\SearchPlace;
use Throwable;

readonly class HomePageController implements Controller
{
    public function __construct(
        private SearchHistory $searchHistory = new SearchHistory(),
        private SearchPlace $searchPlace = new SearchPlace(),
    ) {
    }

    /**
     * @throws Exception
     */
    public function index(): void
    {
        $history = $this->searchHistory->getHistory(Request::ip());

        Response::html('map_page/index', [
            'title'     => 'Map page',
            'header'    => 'Google Map page',
            'latitude'  => Request::get('latitude') ?? LATITUDE_DEFAULT,
            'longitude' => Request::get('longitude') ?? LONGITUDE_DEFAULT,
            'search'    => Request::get('search'),
            'history'   => $history,
        ]);
    }

    public function searchPlaces(): void
    {
        $response = $this->searchPlace->findGoogleAutocompleteAddresses((string) Request::ajaxPost('search'));

        Response::json($response);
    }

    public function redirectToLocation(): void
    {
        $coordinates = $this->searchPlace->getCoordinatesByPlaceId((string) Request::get('placeId'));

        [$latitude, $longitude] = $coordinates;

        if ($latitude && $longitude) {
            $placeName = (string) Request::get('placeName');

            try {
                $this->searchHistory->addHistory(Request::ip(), $latitude, $longitude, $placeName);
            } catch (Throwable) {
            }

            header("Location: /?latitude=$latitude&longitude=$longitude&search=$placeName");

            die;
        }

        header('Location: /');
    }
}
