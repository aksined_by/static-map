<?php

declare(strict_types=1);

if (!defined('IS_APP_STARTED_VALID')) {
    require_once __DIR__ . '/404.php';

    die;
}

spl_autoload_register(function (string $className): void {
    $namespace = str_replace('\\', '/', __NAMESPACE__);
    $className = str_replace('\\', '/', $className);
    $class     = APP_DIR . (empty($namespace) ? '' : "$namespace/") . "$className.php";

    if (file_exists($class)) {
        include($class);
    }
});

function show_404(): void
{
    require_once APP_DIR . '404.php';

    die;
}

function view(string $filename, array $parameters = []): void
{
    $filename = APP_DIR . "Src/view/$filename.php";

    if (!file_exists($filename)) {
        throw new Exception("File $filename not found");
    }

    extract($parameters);
    ob_start();

    include $filename;
    $content = ob_get_clean();

    echo $content;
}
