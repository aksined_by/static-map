<?php

declare(strict_types=1);

use Src\Controller\Main\HomePageController;

if (!defined('IS_APP_STARTED_VALID')) {
    require_once __DIR__ . '/404.php';

    die;
}

const GOOGLE_API_AUTOCOMPLETE_ADDRESS = 'https://maps.googleapis.com/maps/api/place/autocomplete/json';
const GOOGLE_API_ADDRESS_DETAILS      = 'https://maps.googleapis.com/maps/api/place/details/json';
const GOOGLE_API_MAP_STATIC           = 'https://maps.googleapis.com/maps/api/staticmap';

const METHOD_SEPARATOR = '::';

const ROUTES = [
    '/'             => HomePageController::class . METHOD_SEPARATOR . 'index',
    '/searchPlaces' => HomePageController::class . METHOD_SEPARATOR . 'searchPlaces',
    '/showLocation' => HomePageController::class . METHOD_SEPARATOR . 'redirectToLocation',
];
