<?php

declare(strict_types=1);

if (!defined('IS_APP_STARTED_VALID')) {
    require_once __DIR__ . '/404.php';

    die;
}

const APP_DIR = __DIR__ . '/';

const GOOGLE_APY_KEY_MAP = 'AIzaSyCqIBst8tsTmqNgKFmhyVQaYfZl2w_fQJE';

const LATITUDE_DEFAULT  = '53.1080384';
const LONGITUDE_DEFAULT = '23.1495289';

const DB_HOST     = 'mysql';
const DB_USERNAME = 'root';
const DB_PASSWORD = 'root';
const DB_NAME     = 'map';
const DB_PORT     = 3306;